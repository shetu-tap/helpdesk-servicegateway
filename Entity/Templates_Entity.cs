﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceGateway.Entity
{
    public class Templates_Entity
    {
        public int id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string CreatedBy { get; set; }
    }
}
