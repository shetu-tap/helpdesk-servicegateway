﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceGateway.Entity
{
    public class AttachmentsEntity
    {
        public int id { get; set; }
        public string Attachment { get; set; }
        public int Parent { get; set; }
    }
}
