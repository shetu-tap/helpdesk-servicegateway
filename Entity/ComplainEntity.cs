﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceGateway.Entity
{
    public class ComplainEntity
    {
        public int Id { get; set; }
        public string TicketNo { get; set; }
        public string Subject { get; set; }
        public string WalletNo { get; set; }
        public string IssueDetails { get; set; }
        public string Priority { get; set; }
        public string Attachment { get; set; }
        public List<AttachmentsEntity> Attachments { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int AttendBy { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get;  set; }
        public string Status { get; set; }
        public string Parent { get; set; }
    }
}
