﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceGateway.Entity
{
    public class UserEntity
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string EmailAddress { get; set; }
        public string Mobile { get; set; }
        public string Password { get; set; }
        public DateTime DateofBirth { get; set; }
        public string Gender { get; set; }
        public string Country { get; set; }
        public string ChallengeKey { get; set; }
        public string UserType { get; set; }
        public string UserGroup { get; set; }
        public string CreatedBy { get; set; }
        public string UserStatus { get; set; }
    }
}