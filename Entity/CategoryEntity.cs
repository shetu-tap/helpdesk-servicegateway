﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceGateway.Entity
{
    public class CategoryEntity
    {
        public string Category { get; set; }
        public Int16 SLA1 { get; set; }
        public Int16 SLA2 { get; set; }
    }
}
