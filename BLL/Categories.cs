﻿using ServiceGateway.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace ServiceGateway.BLL
{
    public class Categories
    {
        public bool CreateCategory(CategoryEntity categoryEntity)
        {
            CDA db = new CDA();

            DataSet ds = db.GetDataSet("EXEC sp_Create_Complain_Category '" + categoryEntity.Category + "','" + categoryEntity.SLA1 + "','" + categoryEntity.SLA2 + "'", "Complainmgt");

            if (ds != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<CategoryEntity> GetAllCategory()
        {
            CDA db = new CDA();
            List<CategoryEntity> categoryList = new List<CategoryEntity>();
            CategoryEntity categoryEntity;
            DataSet ds = db.GetDataSet("EXEC sp_Get_Complain_Category ", "Complainmgt");

            if (ds != null)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    categoryEntity = new CategoryEntity();
                    categoryEntity.Category = ds.Tables[0].Rows[i].ItemArray[1].ToString();
                    categoryEntity.SLA1 = Convert.ToInt16(ds.Tables[0].Rows[i].ItemArray[2]);
                    categoryEntity.SLA2 = Convert.ToInt16(ds.Tables[0].Rows[i].ItemArray[3]);

                    categoryList.Add(categoryEntity);
                }
                return categoryList;
            }
            else
            {
                return categoryList;
            }
        }
    }

}
