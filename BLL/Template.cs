﻿using MySql.Data.MySqlClient;
using ServiceGateway.DAL;
using ServiceGateway.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Text;

namespace ServiceGateway.BLL
{
    public class Template
    {
        CDA db = new CDA();
        
        public List<Templates_Entity> GetAllTemplate()
        {
            //List<Templates_Entity> list = new List<Templates_Entity>();
            //return list;
            //using (MySqlConnection conn = GetConnection())
            //{
            //    conn.Open();
            //    MySqlCommand cmd = new MySqlCommand("select * from Album where id < 10", conn);

            //    using (var reader = cmd.ExecuteReader())
            //    {
            //        while (reader.Read())
            //        {
            //            list.Add(new Templates_Entity()
            //            {
            //                id = Convert.ToInt32(reader["id"]),
            //                Title = reader["Title"].ToString(),
            //                Description = reader["Description"].ToString(),
            //                CreatedBy = reader["CreatedBy"].ToString()
            //            });
            //        }
            //    }
            //}
            //return list;
            Templates_Entity templatesEntity = new Templates_Entity();
            List<Templates_Entity> templateList = new List<Templates_Entity>();
            DataSet ds = db.GetDataSet("EXEC sp_Get_All_Templates ", "Complainmgt");

            if (ds != null)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    List<AttachmentsEntity> AttachmentList = new List<AttachmentsEntity>();

                    templatesEntity = new Templates_Entity();
                    templatesEntity.id = Convert.ToInt16(ds.Tables[0].Rows[i].ItemArray[0]);
                    templatesEntity.Title = ds.Tables[0].Rows[i].ItemArray[1].ToString();
                    templatesEntity.Description = WebUtility.HtmlDecode(ds.Tables[0].Rows[i].ItemArray[2].ToString());

                    templateList.Add(templatesEntity);
                }
            }
            return templateList;
        }

        public bool CreateTemplate(Templates_Entity templatesEntity)
        {
            CDA db = new CDA();

            DataSet ds = db.GetDataSet("EXEC sp_Create_Template N'" + templatesEntity.Title + "', N'" + templatesEntity.Description.Replace("'", "''") + "','" + templatesEntity.CreatedBy + "'", "Complainmgt");

            if (ds != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

}
