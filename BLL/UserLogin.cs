﻿using ServiceGateway.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace ServiceGateway
{
    public class UserLogin
    {
        CDA db = new CDA();
        public List<UserEntity> Login(UserEntity user)
        {
            UserEntity userEntity = new UserEntity();
            var key = "b14ca5898a4e4133bbce2ea2315a1916";
            var encryptedPassword = CryptoManager.EncryptString(key, user.Password);

            DataSet ds = db.GetDataSet("EXEC sp_Get_User_Login '"+ user.EmailAddress+"', '"+encryptedPassword+"'", "Complainmgt");
            
            if (ds != null)
            {
                List<UserEntity> userList = new List<UserEntity>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    userEntity = new UserEntity();
                    userEntity.Id = Convert.ToInt16(ds.Tables[0].Rows[i].ItemArray[0]);
                    userEntity.FullName = ds.Tables[0].Rows[i].ItemArray[1].ToString();
                    userEntity.EmailAddress = ds.Tables[0].Rows[i].ItemArray[2].ToString();
                    userEntity.Mobile = ds.Tables[0].Rows[i].ItemArray[3].ToString();
                    //userEntity.DateofBirth = ds.Tables[0].Rows[i].ItemArray[3].ToString();
                    userEntity.Gender = ds.Tables[0].Rows[i].ItemArray[5].ToString();
                    userEntity.UserType = ds.Tables[0].Rows[i].ItemArray[6].ToString();

                    userList.Add(userEntity);
                }
                
                return userList;
            }
            else
            {
                return null;
            }
        }

        public List<UserEntity> AgentList()
        {
            UserEntity userEntity = new UserEntity();

            DataSet ds = db.GetDataSet("EXEC sp_Get_All_Agent ", "Complainmgt");

            if (ds != null)
            {
                List<UserEntity> userList = new List<UserEntity>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    userEntity = new UserEntity();
                    userEntity.Id = Convert.ToInt16(ds.Tables[0].Rows[i].ItemArray[0]);
                    userEntity.FullName = ds.Tables[0].Rows[i].ItemArray[1].ToString();
                    userEntity.EmailAddress = ds.Tables[0].Rows[i].ItemArray[2].ToString();
                    userEntity.Mobile = ds.Tables[0].Rows[i].ItemArray[3].ToString();
                    //userEntity.DateofBirth = Convert.ToDateTime(ds.Tables[0].Rows[i].ItemArray[4]);
                    userEntity.Gender = ds.Tables[0].Rows[i].ItemArray[5].ToString();
                    userEntity.UserType = ds.Tables[0].Rows[i].ItemArray[6].ToString();
                    userEntity.UserGroup = ds.Tables[0].Rows[i].ItemArray[7].ToString();
                    userEntity.UserStatus = ds.Tables[0].Rows[i].ItemArray[8].ToString();

                    userList.Add(userEntity);
                }

                return userList;
            }
            else
            {
                return null;
            }
        }

        public List<UserEntity> AgentListById(int Id)
        {
            UserEntity userEntity = new UserEntity();

            DataSet ds = db.GetDataSet("EXEC sp_Get_Agent_List_By_Id '"+ Id + "' ", "Complainmgt");

            if (ds != null)
            {
                List<UserEntity> userList = new List<UserEntity>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    userEntity = new UserEntity();
                    userEntity.Id = Convert.ToInt16(ds.Tables[0].Rows[i].ItemArray[0]);
                    userEntity.FullName = ds.Tables[0].Rows[i].ItemArray[1].ToString();
                    userEntity.EmailAddress = ds.Tables[0].Rows[i].ItemArray[2].ToString();
                    userEntity.Mobile = ds.Tables[0].Rows[i].ItemArray[3].ToString();
                    //userEntity.DateofBirth = Convert.ToDateTime(ds.Tables[0].Rows[i].ItemArray[4]);
                    userEntity.Gender = ds.Tables[0].Rows[i].ItemArray[5].ToString();
                    userEntity.UserType = ds.Tables[0].Rows[i].ItemArray[6].ToString();
                    userEntity.UserGroup = ds.Tables[0].Rows[i].ItemArray[7].ToString();

                    userList.Add(userEntity);
                }

                return userList;
            }
            else
            {
                return null;
            }
        }

        public List<UserEntity> UserProfileById(int Id)
        {
            CryptoManager crypt = new CryptoManager();
            UserEntity userEntity = new UserEntity();
            var keys = "b14ca5898a4e4133bbce2ea2315a1916";

            DataSet ds = db.GetDataSet("EXEC sp_Get_User_Profile_By_Id '" + Id + "' ", "Complainmgt");

            if (ds != null)
            {
                List<UserEntity> userList = new List<UserEntity>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    userEntity = new UserEntity();
                    userEntity.Id = Convert.ToInt16(ds.Tables[0].Rows[i].ItemArray[0]);
                    userEntity.FullName = ds.Tables[0].Rows[i].ItemArray[1].ToString();
                    userEntity.EmailAddress = ds.Tables[0].Rows[i].ItemArray[2].ToString();
                    userEntity.Mobile = ds.Tables[0].Rows[i].ItemArray[3].ToString();
                    //userEntity.DateofBirth = Convert.ToDateTime(ds.Tables[0].Rows[i].ItemArray[4]);
                    userEntity.Gender = ds.Tables[0].Rows[i].ItemArray[5].ToString();
                    userEntity.UserType = ds.Tables[0].Rows[i].ItemArray[6].ToString();
                    userEntity.UserGroup = ds.Tables[0].Rows[i].ItemArray[7].ToString();
                    userEntity.Password = crypt.DecryptString(keys, ds.Tables[0].Rows[i].ItemArray[9].ToString());
                    userEntity.UserStatus = ds.Tables[0].Rows[i].ItemArray[10].ToString();

                    userList.Add(userEntity);
                }

                return userList;
            }
            else
            {
                return null;
            }
        }

        public bool UpdateUser(UserEntity userEntity)
        {
            DataSet ds = db.GetDataSet("EXEC sp_Update_Agent '" + userEntity.Id + "','" + userEntity.FullName + "','" + userEntity.EmailAddress + "','" + userEntity.Mobile + "','" + userEntity.Gender + "','" + userEntity.UserType + "','" + userEntity.UserGroup + "' ", "Complainmgt");
            return true;
        }

        public bool BlockUser(int UserId)
        {
            DataSet ds = db.GetDataSet("EXEC sp_Block_Agent '" + UserId + "' ", "Complainmgt");
            return true;
        }

        public bool DeleteUser(int UserId)
        {
            DataSet ds = db.GetDataSet("EXEC sp_Delete_Agent '" + UserId + "' ", "Complainmgt");
            return true;
        }

    }
}
