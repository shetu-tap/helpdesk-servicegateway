﻿using ServiceGateway.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceGateway.BLL
{
    public class Complain
    {
        CDA db = new CDA();
        public bool CreateComplain(ComplainEntity complain)
        {
            CDA db = new CDA();
            ComplainEntity complainEntity = new ComplainEntity();
            complainEntity.TicketNo = "";

            DataSet ds = db.GetDataSet("EXEC sp_Create_Complain N'" + complain.Subject + "','" + complain.WalletNo + "',N'" + complain.IssueDetails + "','" + complain.Priority + "','" + complain.Attachment + "','" + complain.CreatedBy + "'", "Complainmgt");

            if (ds != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<ComplainEntity> GetAllComplain(string Category)
        {
            CDA db = new CDA();
            ComplainEntity complainEntity = new ComplainEntity();
            complainEntity.TicketNo = "";
            List<ComplainEntity> complainList = new List<ComplainEntity>();
            DataSet ds = db.GetDataSet("EXEC sp_Get_All_Complains_By_Category '" + Category + "'", "Complainmgt");

            if (ds != null)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    List<AttachmentsEntity> AttachmentList = new List<AttachmentsEntity>();

                    complainEntity = new ComplainEntity();
                    complainEntity.Id = Convert.ToInt16(ds.Tables[0].Rows[i].ItemArray[0]);
                    complainEntity.TicketNo = ds.Tables[0].Rows[i].ItemArray[1].ToString();
                    complainEntity.Subject = ds.Tables[0].Rows[i].ItemArray[2].ToString();
                    complainEntity.WalletNo = ds.Tables[0].Rows[i].ItemArray[3].ToString();
                    complainEntity.IssueDetails = ds.Tables[0].Rows[i].ItemArray[4].ToString();
                    complainEntity.Priority = ds.Tables[0].Rows[i].ItemArray[5].ToString();
                    //complainEntity.Attachment = ds.Tables[0].Rows[i].ItemArray[6].ToString();
                    complainEntity.StartTime = ds.Tables[0].Rows[i].ItemArray[10].ToString();
                    complainEntity.EndTime = ds.Tables[0].Rows[i].ItemArray[11].ToString();
                    complainEntity.Status = ds.Tables[0].Rows[i].ItemArray[12].ToString();

                    complainList.Add(complainEntity);
                }
            }
            return complainList;
        }

        /// <summary>
        /// Get list of complain for individual customer
        /// </summary>
        /// <param name="UserWallet"></param>
        /// <returns>List of individual complains</returns>
        public List<ComplainEntity> GetMyComplain(string UserWallet)
        {
            ComplainEntity complainEntity = new ComplainEntity();
            complainEntity.TicketNo = "";
            List<ComplainEntity> complainList = new List<ComplainEntity>();
            DataSet ds = db.GetDataSet("EXEC sp_Get_My_Complains '" + UserWallet + "'", "Complainmgt");

            if (ds != null)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    complainEntity = new ComplainEntity();
                    complainEntity.TicketNo = ds.Tables[0].Rows[i].ItemArray[1].ToString();
                    complainEntity.Subject = ds.Tables[0].Rows[i].ItemArray[2].ToString();
                    complainEntity.WalletNo = ds.Tables[0].Rows[i].ItemArray[3].ToString();
                    complainEntity.IssueDetails = ds.Tables[0].Rows[i].ItemArray[4].ToString();
                    complainEntity.Priority = ds.Tables[0].Rows[i].ItemArray[5].ToString();
                    complainEntity.Attachment = ds.Tables[0].Rows[i].ItemArray[6].ToString();
                    complainEntity.StartTime = ds.Tables[0].Rows[i].ItemArray[10].ToString();
                    complainEntity.EndTime = ds.Tables[0].Rows[i].ItemArray[11].ToString();
                    complainEntity.Status = ds.Tables[0].Rows[i].ItemArray[12].ToString();

                    complainList.Add(complainEntity);
                }

            }

            return complainList;
        }

        public List<ComplainEntity> GetComplainDetails(int id, int AgentId)
        {
            ComplainEntity complainEntity = new ComplainEntity();
            complainEntity.TicketNo = "";
            List<ComplainEntity> complainList = new List<ComplainEntity>();
            DataSet ds = db.GetDataSet("EXEC sp_Get_Complain_Details '" + id + "', '" + AgentId + "'", "Complainmgt");

            if (ds != null)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    List<AttachmentsEntity> AttachmentList = new List<AttachmentsEntity>();
                    complainEntity = new ComplainEntity();
                    complainEntity.Id = Convert.ToInt16(ds.Tables[0].Rows[i].ItemArray[0]);
                    complainEntity.TicketNo = ds.Tables[0].Rows[i].ItemArray[1].ToString();
                    complainEntity.Subject = ds.Tables[0].Rows[i].ItemArray[2].ToString();
                    complainEntity.WalletNo = ds.Tables[0].Rows[i].ItemArray[3].ToString();
                    complainEntity.IssueDetails = ds.Tables[0].Rows[i].ItemArray[4].ToString();
                    complainEntity.Priority = ds.Tables[0].Rows[i].ItemArray[5].ToString();
                    complainEntity.Attachment = ds.Tables[0].Rows[i].ItemArray[6].ToString();
                    AttachmentList = GetAttachments(complainEntity.Id);

                    complainEntity.Attachments = AttachmentList;
                    complainEntity.Parent = ds.Tables[0].Rows[i].ItemArray[7].ToString();
                    complainEntity.CreatedBy = ds.Tables[0].Rows[i].ItemArray[8].ToString();
                    complainEntity.CreatedDate = Convert.ToDateTime(ds.Tables[0].Rows[i].ItemArray[9]);
                    complainEntity.AttendBy = Convert.ToInt16(ds.Tables[0].Rows[i].ItemArray[10]);
                    complainEntity.StartTime = ds.Tables[0].Rows[i].ItemArray[11].ToString();
                    complainEntity.EndTime = ds.Tables[0].Rows[i].ItemArray[12].ToString();
                    complainEntity.Status = ds.Tables[0].Rows[i].ItemArray[13].ToString();

                    complainList.Add(complainEntity);
                }
            }

            return complainList;
        }

        public Task<List<ComplainEntity>> SearchComplainAsync(ComplainEntity cmplnEntity)
        {
            ComplainEntity complainEntity = new ComplainEntity();
            complainEntity.TicketNo = "";
            List<ComplainEntity> complainList = new List<ComplainEntity>();
            DataSet ds = db.GetDataSet("EXEC sp_Search_All_Complains '" + cmplnEntity.TicketNo + "', '" + cmplnEntity.WalletNo + "'", "Complainmgt");

            if (ds != null)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    List<AttachmentsEntity> AttachmentList = new List<AttachmentsEntity>();
                    complainEntity = new ComplainEntity();
                    complainEntity.Id = Convert.ToInt16(ds.Tables[0].Rows[i].ItemArray[0]);
                    complainEntity.TicketNo = ds.Tables[0].Rows[i].ItemArray[1].ToString();
                    complainEntity.Subject = ds.Tables[0].Rows[i].ItemArray[2].ToString();
                    complainEntity.WalletNo = ds.Tables[0].Rows[i].ItemArray[3].ToString();
                    complainEntity.IssueDetails = ds.Tables[0].Rows[i].ItemArray[4].ToString();
                    complainEntity.Priority = ds.Tables[0].Rows[i].ItemArray[5].ToString();
                    complainEntity.Attachment = ds.Tables[0].Rows[i].ItemArray[6].ToString();
                    AttachmentList = GetAttachments(complainEntity.Id);

                    complainEntity.Attachments = AttachmentList;
                    complainEntity.Parent = ds.Tables[0].Rows[i].ItemArray[7].ToString();
                    complainEntity.CreatedBy = ds.Tables[0].Rows[i].ItemArray[8].ToString();
                    complainEntity.CreatedDate = Convert.ToDateTime(ds.Tables[0].Rows[i].ItemArray[9]);
                    complainEntity.AttendBy = Convert.ToInt16(ds.Tables[0].Rows[i].ItemArray[10]);
                    complainEntity.StartTime = ds.Tables[0].Rows[i].ItemArray[11].ToString();
                    complainEntity.EndTime = ds.Tables[0].Rows[i].ItemArray[12].ToString();
                    complainEntity.Status = ds.Tables[0].Rows[i].ItemArray[13].ToString();

                    complainList.Add(complainEntity);
                }
            }

            return Task.FromResult(complainList);
        }

        public bool SubmitReply(ComplainEntity complainEntity)
        {
            CDA db = new CDA();
            DataSet ds = db.GetDataSet("EXEC sp_Reply_Ticket_Answer '" + complainEntity.TicketNo + "',N'" + complainEntity.IssueDetails + "','" + complainEntity.Status + "','" + complainEntity.CreatedBy + "'", "Complainmgt");

            if (ds != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private List<AttachmentsEntity> GetAttachments(int id)
        {
            List<AttachmentsEntity> attachmentList = new List<AttachmentsEntity>();
            AttachmentsEntity attachmentEntity = new AttachmentsEntity();
            DataSet ds = db.GetDataSet("EXEC sp_Get_Complain_Attachments '" + id + "' ", "Complainmgt");
            if (ds != null)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    attachmentEntity = new AttachmentsEntity();
                    attachmentEntity.id = Convert.ToInt16(ds.Tables[0].Rows[i].ItemArray[0]);
                    attachmentEntity.Attachment = ds.Tables[0].Rows[i].ItemArray[1].ToString();
                    attachmentEntity.Parent = Convert.ToInt16(ds.Tables[0].Rows[i].ItemArray[2]);

                    attachmentList.Add(attachmentEntity);
                }
            }

            return attachmentList;
        }

        public bool AttendComplain(string Ticketno, int SessionId)
        {
            CDA db = new CDA();
            DataSet ds = db.GetDataSet("EXEC sp_Attend_Agent_Complain '" + Ticketno + "','" + SessionId + "'", "Complainmgt");

            if (ds != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<ComplainEntity> GetAgentAttendedComplain(int AgentId)
        {
            try
            {
                ComplainEntity complainEntity = new ComplainEntity();
                complainEntity.TicketNo = "";
                List<ComplainEntity> complainList = new List<ComplainEntity>();
                DataSet ds = db.GetDataSet("EXEC sp_Get_Agent_Attendent_Complains '" + AgentId + "'", "Complainmgt");

                if (ds != null)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        complainEntity = new ComplainEntity();
                        complainEntity.Id = Convert.ToInt16(ds.Tables[0].Rows[i].ItemArray[0]);
                        complainEntity.TicketNo = ds.Tables[0].Rows[i].ItemArray[1].ToString();
                        complainEntity.Subject = ds.Tables[0].Rows[i].ItemArray[2].ToString();
                        complainEntity.WalletNo = ds.Tables[0].Rows[i].ItemArray[3].ToString();
                        complainEntity.IssueDetails = ds.Tables[0].Rows[i].ItemArray[4].ToString();
                        complainEntity.Priority = ds.Tables[0].Rows[i].ItemArray[5].ToString();
                        complainEntity.Attachment = ds.Tables[0].Rows[i].ItemArray[6].ToString();
                        complainEntity.Parent = ds.Tables[0].Rows[i].ItemArray[7].ToString();
                        complainEntity.CreatedBy = ds.Tables[0].Rows[i].ItemArray[8].ToString();
                        complainEntity.CreatedDate = Convert.ToDateTime(ds.Tables[0].Rows[i].ItemArray[9]);
                        complainEntity.AttendBy = Convert.ToInt16(ds.Tables[0].Rows[i].ItemArray[10]);
                        complainEntity.StartTime = ds.Tables[0].Rows[i].ItemArray[11].ToString();
                        complainEntity.EndTime = ds.Tables[0].Rows[i].ItemArray[12].ToString();
                        complainEntity.Status = ds.Tables[0].Rows[i].ItemArray[13].ToString();

                        complainList.Add(complainEntity);
                    }
                }

                return complainList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        //public bool AgentReply(int Id)
        //{
        //    try
        //    {
        //        ComplainEntity complainEntity = new ComplainEntity();
        //        complainEntity.TicketNo = "";
        //        List<ComplainEntity> complainList = new List<ComplainEntity>();
        //        DataSet ds = db.GetDataSet("EXEC sp_Get_Agent_Attendent_Complains '" + AgentId + "'", "Complainmgt");

        //        if (ds != null)
        //        {

        //        }

        //        return complainList;
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}
    }
}
