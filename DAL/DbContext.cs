﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Text;


namespace ServiceGateway.DAL
{
    public class DbContext
    {
        public MySqlConnection ConnectionString { get; set; }
        public DbContext(string ConnString)
        {
            MySqlConnection cnn;

            cnn = new MySqlConnection(ConnString);
            try
            {
                cnn.Open();
                //cnn.Close();
            }
            catch (Exception ex)
            {
                throw ex;
                //MessageBox.Show("Can not open connection ! ");
            }
            this.ConnectionString = cnn;
        }

    }
}
